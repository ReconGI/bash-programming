#!/bin/bash
# usf0 - script work
if [ $# -gt 3 ]; then
   echo "Woah now, please use 3 maximum"
   exit 1

elif [ $# -eq 3 ]; then
     echo "Signed Functionality Isn't Implemented"
     exit 1

elif [ $# -eq 2 ]; then
     let startNum=$1
     let endNum=$2

elif [ $# -eq 1 ]; then
     let startNum=$1
     let endNum=$1+15
else
     let startNum=0
     let endNum=$startNum+15
fi

for i in "base 2" "base 8" "base 10"; do
      printf "  %-8s|" "$i"
done
printf "  %-8s  \n" "base16"

printf "%s\n" "----------+----------+----------+----------"

#I know I need to loop through startNum and endNum in order to display this:

for (( i=$startNum; i<$endNum+1; i++ )); do

if [ $i -le 15 ]; then
   Base16Filler="0"
else
   Base16Filler=""
fi



if [ $i -le 7 ]; then
   Base8Filler="00"
else
   Base8Filler="0"
fi



if [ $i -le 1 ]; then
   Base2Filler="000"
elif [ $i -le 3 ]; then
   Base2Filler="00"
else
   Base2Filler="0"
fi



let base2=$(echo "obase=2; ibase=10; $i" | bc)



if [ $i -le 1 ]; then
   printf "%8s%d |" "$Base2Filler" "$base2"
elif [ $i -le 3 ]; then
   printf "%7s%d |" "$Base2Filler" "$base2"
elif [ $i -le 7 ]; then
   printf "%6s%d |" "$Base2Filler" "$base2"
else
   printf "%9d |" "$base2"
fi



if [ $i -le 7 ]; then
   printf "%8s%o |" "$Base8Filler" "$i"
else
   printf "%7s%o |" "$Base8Filler" "$i"
fi
printf "%9d |" "$i"



if [ $i -le 15 ]; then
   printf "%8s%X\n" "0x$Base16Filler" "$i"
else
   printf "%7s%X\n" "0x$Base16Filler" "$i"
fi
done

exit 0

# base 2   |  base 8  |  base10  |  base16
#----------+----------+----------+----------
#     0000 |      000 |        0 |     0x00
#     0001 |      001 |        1 |     0x01
#     0010 |      002 |        2 |     0x02
#     0011 |      003 |        3 |     0x03
#     0100 |      004 |        4 |     0x04
#     0101 |      005 |        5 |     0x05
#     0110 |      006 |        6 |     0x06
#     0111 |      007 |        7 |     0x07
#     1000 |      010 |        8 |     0x08
#     1001 |      011 |        9 |     0x09
#     1010 |      012 |       10 |     0x0A
#     1011 |      013 |       11 |     0x0B
#     1100 |      014 |       12 |     0x0C
#     1101 |      015 |       13 |     0x0D
#     1110 |      016 |       14 |     0x0E
#     1111 |      017 |       15 |     0x0F

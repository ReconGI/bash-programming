#!/bin/bash
#
# arrayfun.sh - play with arrays for fun and profit
#
index=0
data[${index}]="things"
let index=index+1
data[${index}]=80
let index=index+1
data[2]=3.1337
let index=index+2

for index in 0 1 2; do
    echo "#${index} is: ${data[${index}]}"
done

exit 0

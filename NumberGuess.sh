#!/bin/bash
#
#hilow - a simple number guessing game
#

biggest=100                             # maximum number possible
guess=0                                 # guessed by player
guesses=0                               # number of guesses made
number=$(( $$ % $biggest ))             # random number, 1 .. $biggest

while [ $guess -ne $number ] ; do
  echo -n "Guess? " ; read guess
  if [ "$guess" -lt $number ] ; then
    echo "... Think Larger Bro!"
  elif [ "$guess" -gt $number ] ; then
    echo "... Take it down a notch, you are closer!"
  fi
  guesses=$(( $guesses + 1 ))
done

echo "OMG AMAZING you were Right!! Guessed $number in $guesses guesses."

exit 0